$ErrorActionPreference = "Stop"

echo "Running on $env:computername..."
echo $(Get-ChildItem Env:)
echo $(Get-ChildItem Env:) |select-object -ExpandProperty Value

& {
    Update-ModuleManifest .\STtools.psd1 -ModuleVersion $env:CI_COMMIT_TAG
    Publish-Module -path . -NuGetApiKey $env:NuGetApiKey
}
if(!$?) { Exit $LASTEXITCODE }