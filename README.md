# Tools for school (and other) system admins

## ...Especially those that have to deal with CSV for everything

### Get-STGroupsFromCSV

Outputs objects with an "Identity" (group name), "Members" (users who should be in the group named in "Identity"), and "NonMembers" (users who should not be in the group named in "Identity") based on a given CSV (plus prefix/postfix options to flexibly create group names).  This can be used to ensure that users are in the groups they should be in, and aren't on those they shouldn't by piping these objects through a `ForEach-Object` and two simple commands.  See the examples below for how this can be used.

### ConvertFrom-STEduHubSF / ConvertFrom-STEduHubST

Converts CASES EduHub format Staff/Student CSV files into objects that can be piped into New-ADUser

### Set-STUser

Update user account properties to match the incoming ADUser compatible object to ensure things like changed names are updated.  Also Creates a home directory path if it does not exist, and gives the user Full Control of that directory.

### New-STUser

Wraps New-ADUser, adding complex pseudo-random password generation, home directory creation and permission setting, and an email report

### Set-STOrganizationalUnit

Moves users to the OU specified in the userOUs parameter (a Hashtable of OU => @(<ADUser objects>))

## Examples

### `Get-STGroupsFromCSV`

#### Student.csv

| STKEY | STATUS | SCHOOL_YEAR | ... |
| --- | --- | ---: | --- |
| ABC0001 | ACTV | 07 | ... |
| ABC0002 | ACTV | 08 | ... |
| ... | ... | ... | ... |

#### Example Command

```powershell
Get-STGroupsFromCSV -csvfile "\\path\to$\Student.csv" -prefix "Year" -username_header "STKEY" -group_header "SCHOOL_YEAR" -stripzeros -filter {$_.STATUS -eq "ACTV"}

```

#### Example output

(list of objects, formatted as a table)

| Identity | Members | NonMembers |
| --- | --- | --- |
| Year7 | ```@('ABC0001', ...)``` | ```@('ABC0002', <all other users in AD>)```
| Year8 | ```@('ABC0002', ...)``` | ```@('ABC0001', <all other users in AD>)```

#### Example usage

```powershell
Get-STGroupsFromCSV `
    -csvfile "\\path\to$\STUDENT.csv" `
    -prefix "Year" `
    -username_header "STKEY" `
    -group_header "SCHOOL_YEAR" `
    -stripzeros `
    -filter {$_.STATUS -eq "ACTV"} |
        ForEach-Object {
            Remove-ADGroupMember `
                -Identity $_.Identity `
                -Members $_.NonMembers `
                -Confirm:$false -PassThru
            Add-ADGroupMember `
                -Identity $_.Identity `
                -Members $_.Members `
                -Confirm:$false `
                -PassThru
        }
```

### More Get-STGroupsFromCSV examples...

```powershell
# Year level groups for staff
# Staff are assigned to groups according to the year levels of classes in their timetable
# eg. 7teachers, 8teachers ...
Get-STGroupsFromCSV `
    -csvfile "\\path\to\classes\taught.csv" `
    -username_header "Teacher Code" `
    -group_header "Roll Class Code" `
    -postfix teachers `
    -onlynumbers |ForEach-Object {
        Remove-ADGroupMember `
            -Identity $_.Identity `
            -Members $_.NonMembers `
            -Confirm:$false `
            -PassThru
        Add-ADGroupMember `
            -Identity $_.Identity `
            -Members $_.Members `
            -Confirm:$false `
            -PassThru
    }

# Staff group
# Staff are assigned to the Staff group according to them being active in CASES
Get-STGroupsFromCSV -csvfile "\\path\to\file\with\staff.csv" -username_header "SFKEY" -group_name "Staff" -filter {$_.STAFF_STATUS -eq "ACTV"} |ForEach-Object {
    Remove-ADGroupMember -Identity $_.Identity -Members $_.NonMembers -Confirm:$false -PassThru
    Add-ADGroupMember -Identity $_.Identity -Members $_.Members -Confirm:$false -PassThru
}


# Class groups for students
# Students are assigned to groups according to classes they have in timetabler
# eg. 07ENG1, 08ENG3, ...
Get-STGroupsFromCSV -csvfile "\\path\to\file\with\classes.csv" -username_header "Student Code" -group_header "Class Code" |ForEach-Object {
    Remove-ADGroupMember -Identity $_.Identity -Members $_.NonMembers -Confirm:$false -PassThru
    Add-ADGroupMember -Identity $_.Identity -Members $_.Members -Confirm:$false -PassThru
}

```

### `ConvertFrom-STEduHubSF / ConvertFrom-STEduHubST`

#### SF_0000.csv

| SFKEY | STAFF_STATUS | FIRST_NAME | SURNAME | ... |
| --- | --- | --- | --- | --- |
| GJ00 | ACTV | George | Jones | ... |
| JS00 | ACTV | John | Smith | ... |
| ... | ... | ... | ... | ... |

#### Example Command

```powershell
Import-Csv SF_0000.csv |
    ConvertFrom-STEduHubSF -HomeDirBase $HomeBase `
                           -Domain $myDomainName `
                           -HomeDrive H:

```

#### Example output

(objects that can be piped to `New-ADUser` or `New-STUser`, or used in `Set-STUser`)

| DisplayName | EmailAddress | Enabled | GivenName | HomeDirectory | HomeDrive | Name | PasswordNotRequired | SamAccountName | Surname | UserPrincipalName |
| --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| George Jones | GJ00@mydomain.com | $true | George | ```\\fs1\homes\GJ00``` | H: | GJ00 | $false | GJ00 | Jones | GJ00@mydomain.com |
| John Smith | JS00@mydomain.com | $true | John | ```\\fs1\homes\JS00``` | H: | JS00 | $false | JS00 | Smith | JS00@mydomain.com |
| ... | ... | ... | ... | ... | ... | ... | ... | ... | ... | ... |

#### Example Usage

```powershell
Import-Csv $staffcsv |
    Where-Object {
        ($_.STAFF_STATUS -eq "ACTV" -or $_.SFKEY -in $manualActiveStaff.SFKEY) `
        -and `
        ($_.SFKEY -notin $manualInactiveStaff.SFKEY)
    } |
    ConvertFrom-STEduHubSF -HomeDirBase $HomeBase -Domain $myDomainName -HomeDrive U: | New-ADUser

```

### `Set-STUser`

#### Example input

```powershell
$staffUsers = Import-Csv $staffCsvFile |
    ConvertFrom-STEduHubSF -HomeDirBase $HomeBase -Domain $myDomainName -HomeDrive H:
$staffHomeDirBase = \\fs1\homes\
$allUsers = Get-ADUser -filter * -properties *
# you should carefully select properties you care to update if you have a big directory (we only have a few thousand users, so performance is fine using this lazy method)
```

#### Example Usage

```powershell
Set-STUser -users $staffUsers -HomeDirBase $staffHomeDirBase -allUsers $allUsers
```