﻿$here = Split-Path -Parent (Split-Path -Parent $MyInvocation.MyCommand.Path)
$sut = ((Split-Path -Leaf $MyInvocation.MyCommand.Path) -replace '\.Tests\.', '.') -replace '\.ps1', '.psm1'
get-module sttools |remove-module
import-module "$here\$sut" -Force

#Describe "Get-STGroupsFromCSV" {
#    It "does something useful" {
#        $true | Should -Be $false
#    }
#}

Describe "Unit Tests for ConvertFrom-STEduHubST" -Tag "Unit" {
    $fakeUser = [PSCustomObject]@{
        FIRST_NAME = "George";
        SURNAME = "Jones";
        STKEY = "JON0001";
        STATUS = "ACTV";
        BIRTHDATE = "15/07/2001 12:00:00 AM";
    }
    $result = ConvertFrom-STEduHubST -UserList $fakeUser -HomeDirBase \\abcd1234\ -Domain blah.com -HomeDrive H:
    It "Produces correct display name" {
        $result.DisplayName |Should -Match "George Jones"
    }
    It "Produces correct email address" {
        $result.EmailAddress |Should -Match "jon0001@blah.com"
    }
    It "Produces correct enablement" {
        $result.Enabled |Should -Match $true
    }
    It "Produces correct given name" {
        $result.GivenName |Should -Match "George"
    }
    It "Produces correct home directory" {
        $result.HomeDirectory |Should -Contain "\\abcd1234\jon0001"
    }
    It "Produces correct home drive" {
        $result.HomeDrive |Should -Match "H:"
    }
    It "Produces correct Name" {
        $result.Name |Should -Match "JON0001"
    }
    It "Produces correct SamAccountName" {
        $result.SamAccountName |Should -Match "jon0001"
    }
    It "Produces correct Surname" {
        $result.Surname |Should -Match "Jones"
    }
    It "Produces correct UPN" {
        $result.UserPrincipalName |Should -Match ""
    }
}


Describe "Select-STAccountsToDisable" {
    Context "Source object Enabled=`$false and target object Enabled=`$true" {
        $sourceUser = [PSCustomObject]@{
            SamAccountName = "faketestuser";
            Enabled = $false
        }
        $aduser = [PSCustomObject]@{
            SamAccountName = "faketestuser";
            Enabled = $true
        }
        $accountstodisable = $sourceUser |Select-STAccountsToDisable -ADUsers $aduser
        It "Select source objects with Enabled=`$false where target Enabled=`$true" {
            $accountstodisable.Enabled | Should -Be $false
        }
    }
    Context "Source object Enabled=`$true and target object Enabled=`$false" {
        $sourceUser = [PSCustomObject]@{
            SamAccountName = "faketestuser";
            Enabled = $true
        }
        $aduser = [PSCustomObject]@{
            SamAccountName = "faketestuser";
            Enabled = $false
        }
        $accountstodisable = $sourceUser |Select-STAccountsToDisable -ADUsers $aduser
        It "Doesn't output accounts that have Enabled=`$true" {
            $accountstodisable | Should -Be $null
        }
    }
    Context "Source object Enabled=`$false and target object Enabled=`$false" {
        $sourceUser = [PSCustomObject]@{
            SamAccountName = "faketestuser";
            Enabled = $false
        }
        $aduser = [PSCustomObject]@{
            SamAccountName = "faketestuser";
            Enabled = $false
        }
        $accountstodisable = $sourceUser |Select-STAccountsToDisable -ADUsers $aduser
        It "Doesn't output accounts that are already disabled in AD" {
            $accountstodisable | Should -Be $null
        }
    }
    Context "Source object Enabled=`$false and no target object" {
        $sourceUser = [PSCustomObject]@{
            SamAccountName = "faketestuser";
            Enabled = $false
        }
        $aduser = [PSCustomObject]@{
            SamAccountName = "notmatchingfaketestuser";
            Enabled = $true
        }
        $accountstodisable = $sourceUser |Select-STAccountsToDisable -ADUsers $aduser
        It "Doesn't output accounts that don't exist in AD" {
            $accountstodisable | Should -Be $null
        }
    }
    Context "IgnoreList passed in" {
        $sourceUser = [PSCustomObject]@{
            SamAccountName = "faketestuser";
            Enabled = $false
        }
        $aduser = [PSCustomObject]@{
            SamAccountName = "faketestuser";
            Enabled = $true
        }
        $accountstodisable = $sourceUser |Select-STAccountsToDisable -ADUsers $aduser -ignoreList @('faketestuser')
        It "Doesn't output accounts that are in an ignorelist" {
            $accountstodisable | Should -Be $null
        }
    }
    Context "forceInactive list passed in" {
        $sourceUser = [PSCustomObject]@{
            SamAccountName = "faketestuser";
            Enabled = $true
        }
        $aduser = [PSCustomObject]@{
            SamAccountName = "faketestuser";
            Enabled = $true
        }
        $accountstodisable = $sourceUser |Select-STAccountsToDisable -ADUsers $aduser -forceInactiveList @('faketestuser')
        It "Output accounts that are in an forceInactiveList even if they were Enabled" {
            $accountstodisable.SamAccountName | Should -Be "faketestuser"
        }
        It "Sets the account's Enabled status to `$false" {
            $accountstodisable.Enabled | Should -Be $false
        }
    }
    Context "forceInactiveList and ignoreList passed in" {
        $sourceUser = [PSCustomObject]@{
            SamAccountName = "faketestuser";
            Enabled = $true
        }
        $aduser = [PSCustomObject]@{
            SamAccountName = "faketestuser";
            Enabled = $true
        }
        $accountstodisable = $sourceUser |Select-STAccountsToDisable -ADUsers $aduser -forceInactiveList @('faketestuser') -ignoreList @('faketestuser')
        It "Output accounts that are in an forceInactiveList even if they were Enabled and in ignoreList" {
            $accountstodisable.SamAccountName | Should -Be "faketestuser"
        }
    }
}